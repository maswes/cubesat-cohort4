**********************Simulation Dependencies**********************
1- linux Operating system (Ubuntu 14.04 LTS)
2- python 2.7
3- Gnuradio/Gnuradio Companion
*******************************************************************


**********************Simulation Instructions**********************
0- open 2.grc in Gnuradio Companion and run
1- run sudo ./modem (or ./modem_hex) 
2- run python configFsk.py (or configBpsk.py)
3- run udpstream.py
******************************************************************* 


