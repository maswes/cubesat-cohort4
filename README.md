# README #

Instruction Manual to run the Demo

### What is this repository for? ###

* Cube-Sat baseband processing daemon demo.
* Version 0.1

### Simulation Dependencies ###
* linux Operating system (Ubuntu 14.04 LTS)
* python 2.7
* Gnuradio/Gnuradio Companion


### Simulation Instructions ###
* open 2.grc in Gnuradio Companion and run
* run sudo ./modem (or ./modem_hex) 
* run python configFsk.py (or configBpsk.py)
* run udpstream.py