#!/usr/bin/env python2
##################################################
# GNU Radio Python Flow Graph
# Title: Top Block
# Generated: Thu Apr 14 19:17:59 2016
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import wxgui
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from gnuradio.wxgui import fftsink2
from gnuradio.wxgui import forms
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class top_block(grc_wxgui.top_block_gui):

    def __init__(self):
        grc_wxgui.top_block_gui.__init__(self, title="Top Block")
        _icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
        self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

        ##################################################
        # Variables
        ##################################################
        self.variable_slider_0 = variable_slider_0 = 0
        self.samp_rate = samp_rate = 76800

        ##################################################
        # Blocks
        ##################################################
        _variable_slider_0_sizer = wx.BoxSizer(wx.VERTICAL)
        self._variable_slider_0_text_box = forms.text_box(
        	parent=self.GetWin(),
        	sizer=_variable_slider_0_sizer,
        	value=self.variable_slider_0,
        	callback=self.set_variable_slider_0,
        	label='variable_slider_0',
        	converter=forms.float_converter(),
        	proportion=0,
        )
        self._variable_slider_0_slider = forms.slider(
        	parent=self.GetWin(),
        	sizer=_variable_slider_0_sizer,
        	value=self.variable_slider_0,
        	callback=self.set_variable_slider_0,
        	minimum=0,
        	maximum=2,
        	num_steps=100,
        	style=wx.SL_HORIZONTAL,
        	cast=float,
        	proportion=1,
        )
        self.Add(_variable_slider_0_sizer)
        self.wxgui_fftsink2_0 = fftsink2.fft_sink_c(
        	self.GetWin(),
        	baseband_freq=0,
        	y_per_div=10,
        	y_divs=10,
        	ref_level=0,
        	ref_scale=2.0,
        	sample_rate=samp_rate,
        	fft_size=1024,
        	fft_rate=1000,
        	average=False,
        	avg_alpha=None,
        	title="FFT Plot",
        	peak_hold=False,
        )
        self.Add(self.wxgui_fftsink2_0.win)
        self.blocks_udp_source_1 = blocks.udp_source(gr.sizeof_char*1, "127.0.0.1", 5005, 256, True)
        self.blocks_udp_source_0 = blocks.udp_source(gr.sizeof_short*1, "127.0.0.1", 2003, 256, False)
        self.blocks_udp_sink_0_0 = blocks.udp_sink(gr.sizeof_short*1, "127.0.0.1", 2004, 256, False)
        self.blocks_udp_sink_0 = blocks.udp_sink(gr.sizeof_char*1, "127.0.0.1", 2002, 256, False)
        self.blocks_short_to_float_1 = blocks.short_to_float(1, 32768.0)
        self.blocks_short_to_float_0 = blocks.short_to_float(1, 32768.0)
        self.blocks_interleave_0 = blocks.interleave(gr.sizeof_short*1, 1)
        self.blocks_float_to_short_1 = blocks.float_to_short(1, 32767.0)
        self.blocks_float_to_short_0 = blocks.float_to_short(1, 32767.0)
        self.blocks_float_to_complex_0 = blocks.float_to_complex(1)
        self.blocks_deinterleave_0 = blocks.deinterleave(gr.sizeof_short*1, 1)
        self.blocks_add_xx_1 = blocks.add_vff(1)
        self.blocks_add_xx_0 = blocks.add_vff(1)
        self.analog_noise_source_x_1 = analog.noise_source_f(analog.GR_GAUSSIAN, variable_slider_0, 8754)
        self.analog_noise_source_x_0 = analog.noise_source_f(analog.GR_GAUSSIAN, variable_slider_0, 12845)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_noise_source_x_0, 0), (self.blocks_add_xx_0, 1))    
        self.connect((self.analog_noise_source_x_1, 0), (self.blocks_add_xx_1, 1))    
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_float_to_complex_0, 0))    
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_float_to_short_0, 0))    
        self.connect((self.blocks_add_xx_1, 0), (self.blocks_float_to_complex_0, 1))    
        self.connect((self.blocks_add_xx_1, 0), (self.blocks_float_to_short_1, 0))    
        self.connect((self.blocks_deinterleave_0, 0), (self.blocks_short_to_float_0, 0))    
        self.connect((self.blocks_deinterleave_0, 1), (self.blocks_short_to_float_1, 0))    
        self.connect((self.blocks_float_to_complex_0, 0), (self.wxgui_fftsink2_0, 0))    
        self.connect((self.blocks_float_to_short_0, 0), (self.blocks_interleave_0, 0))    
        self.connect((self.blocks_float_to_short_1, 0), (self.blocks_interleave_0, 1))    
        self.connect((self.blocks_interleave_0, 0), (self.blocks_udp_sink_0_0, 0))    
        self.connect((self.blocks_short_to_float_0, 0), (self.blocks_add_xx_0, 0))    
        self.connect((self.blocks_short_to_float_1, 0), (self.blocks_add_xx_1, 0))    
        self.connect((self.blocks_udp_source_0, 0), (self.blocks_deinterleave_0, 0))    
        self.connect((self.blocks_udp_source_1, 0), (self.blocks_udp_sink_0, 0))    


    def get_variable_slider_0(self):
        return self.variable_slider_0

    def set_variable_slider_0(self, variable_slider_0):
        self.variable_slider_0 = variable_slider_0
        self.analog_noise_source_x_1.set_amplitude(self.variable_slider_0)
        self.analog_noise_source_x_0.set_amplitude(self.variable_slider_0)
        self._variable_slider_0_slider.set_value(self.variable_slider_0)
        self._variable_slider_0_text_box.set_value(self.variable_slider_0)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.wxgui_fftsink2_0.set_sample_rate(self.samp_rate)


if __name__ == '__main__':
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    (options, args) = parser.parse_args()
    tb = top_block()
    tb.Start(True)
    tb.Wait()
