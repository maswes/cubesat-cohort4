import socket
from  modeminterface import *
import time

SAMPLE_RATE		=	76800
DATA_RATE		=	9600
OVER_SAMPLING		=	8
MODULATION_TYPE		=	1
LSB_MSB			=	0
BFSK_DEVIATION		=	2000	
FREQUENCY_OFFSET	=	0

UDP_PORT_TX=2000
UDP_PORT_RX=2001
UDP_IP="127.0.0.1"

sock1 = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP

mymodem=modem() 

packet=mymodem.setRxSampleRateCommand(SAMPLE_RATE)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.setRxDataRateCommand(DATA_RATE)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.setRxOverSamplingCommand(OVER_SAMPLING)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.setRxDemodulationTypeCommand(MODULATION_TYPE)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.setRxMsbLsbOutCommand(LSB_MSB)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.setRxBfskFrequencyDeviationCommand(BFSK_DEVIATION)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.setTxSampleRateCommand(SAMPLE_RATE)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.setTxDataRateCommand(DATA_RATE)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.setTxOverSamplingCommand(OVER_SAMPLING)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.setTxModulationTypeCommand(MODULATION_TYPE)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.setTxFrequencyCommand(FREQUENCY_OFFSET)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.setTxBfskFrequencyDeviationCommand(BFSK_DEVIATION)
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.rxChainRestartCommand()
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))

packet=mymodem.txChainRestartCommand()
sock1.sendto(packet, (UDP_IP, UDP_PORT_TX))
